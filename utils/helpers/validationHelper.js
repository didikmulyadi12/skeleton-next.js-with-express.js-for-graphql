export default class ValidationHelper {
    static justNumber   = function(varString) {
        return varString.toString().replace(/\D+/g,'');
    }

    static phoneNumber = function(varString) {
        return varString.toString().replace(/^0|\D+/g,'');
    }

    static justAlphabet = function(varString) {
        return varString.toString().replace(/[^A-Za-z\s]/g,'');
    }

    static postCodeMax  = function(varString) {
        return varString.toString().substring(0,5);
    }

    static ktpNumberMax = function(varString) {
        return varString.toString().substring(0,16);
    }

    static npwpNumberMax    =   function(varString) {
        return varString.toString().substring(0,15);
    }

    static phoneNumberMax   =   function(varString) {
        return varString.toString().substring(0,12);
    }
}