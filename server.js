// server.js
const next = require('next')
const routes = require('./routes')
const app = next({dev: process.env.NODE_ENV !== 'production'})
const port = process.env.NODE_ENV !== 'production' ? 3000 : 80;
const handler = routes.getRequestHandler(app)

// With express
const express = require('express')

app.prepare().then(() => {

  const server = express()
  
  server.use(handler).listen(port)
})

