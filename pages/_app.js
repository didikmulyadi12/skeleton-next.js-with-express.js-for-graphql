import React from "react";
import {Provider} from "react-redux";
import App, {Container} from "next/app";
import withRedux from "next-redux-wrapper";
import makeStore from "../lib/redux/store"
import {PersistGate} from 'redux-persist/integration/react';
import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import fetch from 'node-fetch'

class MyApp extends App {
    
    render(){
        
        const {Component, pageProps, store} = this.props;
        const client = new ApolloClient({
            link : new HttpLink({
              uri : "https://48p1r2roz4.sse.codesandbox.io",
              fetch : fetch
            }),
            cache : new InMemoryCache()
        });

        return (
            <Container>
                <Provider store={store}>
                    <PersistGate persistor={store.__persistor} loading={""}>
                        <ApolloProvider client={client}>
                            <Component {...pageProps} />
                        </ApolloProvider>
                    </PersistGate>
                </Provider>
                
            </Container>
        );
    }
}

export default withRedux(makeStore, {debug: true})(MyApp);