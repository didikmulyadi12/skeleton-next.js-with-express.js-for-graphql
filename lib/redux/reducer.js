import {
    SET_CLIENT_STATE
} from './names'

const reducer = (state, {type, payload}) => {
    switch (type) {
        case 'FOO':
            return {...state, foo: payload};
        case SET_CLIENT_STATE : 
            return { ...state, fromClient: payload };
        default:
            return state
    }
};

export default reducer;